# Malvarmo — A Monero cold storage wallet generator

[![Build Status](https://travis-ci.org/leonklingele/malvarmo.svg?branch=master)](https://travis-ci.org/leonklingele/malvarmo)

Malvarmo — meaning "cold" in Esperanto — is a tiny cold storage wallet generator written in Go. It is intended for educational purposes only. __Please don't use it to store any Moneroj.__
